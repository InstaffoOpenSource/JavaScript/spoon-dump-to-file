import test from 'p-tape'
import eq from 'equal-pmb'

import prettyPrint from '../prettyprint'

test('Cope with circular HTTP reply', async(t) => {
  const reply = {
    statusCode: 200,
    statusMessage: 'OK',
    headers: { Date: 'today' },
    body: 'Hello World',
    history: [],
    _underscore_prefixed: 'ignore me',
  }
  reply.history.push(reply)
  const pretty = prettyPrint(reply)
  eq.lines(pretty, [
    '{',
    '  "history": [',
    '    {', // <-- not a loop yet: root object is just a partial copy of reply
    '      "_underscore_prefixed": "ignore me",',
    '      "body": "Hello World",',
    '      "headers": {',
    '        "Date": "today"',
    '      },',
    '      "history": "[Circular ~.history]",',
    '      "statusCode": 200,',
    '      "statusMessage": "OK"',
    '    }',
    '  ],',
    '  "statusCode": 200,',
    '  "statusMessage": "OK"',
    '}',
    '',
    '200 OK',
    'Date: today',
    '',
    'Hello World',
  ])
  t.ok(true)
})
