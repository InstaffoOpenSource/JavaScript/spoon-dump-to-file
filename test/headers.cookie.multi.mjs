import test from 'p-tape'
import eq from 'equal-pmb'

import prettyPrint from '../prettyprint'

test('Cope with multiple new cookies', async(t) => {
  const reply = {
    statusCode: 200,
    statusMessage: 'OK',
    headers: {
      'set-cookie': [
        'foo=123;secure;httponly; path=/',
        'bar=456; path=/; HttpOnly',
      ],
      connection: 'close',
      server: 'Apache',
    },
    body: 'Hello World',
  }
  const pretty = prettyPrint(reply)
  eq.lines(pretty, [
    '{',
    '  "statusCode": 200,',
    '  "statusMessage": "OK"',
    '}',
    '',
    '200 OK',
    'connection: close',
    'server: Apache',
    'set-cookie: foo=123;secure;httponly; path=/',
    'set-cookie: bar=456; path=/; HttpOnly',
    '',
    'Hello World',
  ])
  t.ok(true)
})
