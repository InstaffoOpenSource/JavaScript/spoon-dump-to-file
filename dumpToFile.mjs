import promisedFs from 'nofs'

import prettyPrint from './prettyprint'

function randStr() { return (Math.random().toString(36).slice(2) || '0') }

async function dumpToFile(destPattern, x) {
  const data = prettyPrint(x)
  const nowPid = `${Date.now().toString(36)}.${process.pid}`
  const dest = destPattern.replace(/\n/g, nowPid).replace(/\r/g, randStr)
  await promisedFs.writeFile(dest, data)
  return { dest, data }
}

export default dumpToFile
