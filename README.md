﻿
<!--#echo json="package.json" key="name" underline="=" -->
spoon-dump-to-file
==================
<!--/#echo -->

<!--#echo json="package.json" key="description" -->
HTTP-aware general purpose dumper.
<!--/#echo -->

* 📣 💼 💰 Looking for a tech job?
  Try our [reverse recruiting service](https://instaffo.com/).



API
---

This module exports one function:

### dumpToFile(filename, x)

Tries to dump `x` to `filename`, guessing the best format.
Returns a promise for an object `{ data, dest }` where
`data` is the text representation of `x`,
and `dest` is the effective filename to which `data` was saved.

The effective filename differs from `filename` if any of the magic is used:
* All `\n` (non-collision) are replaced with a hopefully-unique ID.
* All `\r` (random) are replaced with a random alphanumeric string.



<!--#toc stop="scan" -->


&nbsp;

License
-------
<!--#echo json="package.json" key=".license" -->
MIT
<!--/#echo -->
