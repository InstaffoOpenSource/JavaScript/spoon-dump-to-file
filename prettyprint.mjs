import util from 'util'
import univeil from 'univeil'
import deepSortObj from 'deepsortobj'
import isObj from 'is-obj'
import ensureArray from 'ensure-array'

const objToStr = Object.prototype.toString

function headSep(x, i) { return x + headSep[i % 2] }
headSep[0] = ': '
headSep[1] = '\n'
const errNoHeaders = '(Found no HTTP headers.)'

function dumpHttpHeaders(hdr) {
  if (!hdr) { return errNoHeaders }
  if (Array.isArray(hdr)) {
    if (!hdr.length) { return errNoHeaders }
    return hdr.map(headSep).join('')
  }
  const names = Object.keys(hdr).sort()
  const list = []
  names.forEach(function dumpOneHeaderKey(key) {
    list.push(...ensureArray(hdr[key]).map(val => `${key}: ${val}`))
  })
  return list.join('\n')
}


function lowerSingular(word) {
  return word.toLowerCase().replace(/s$/, '')
}

const httpExtractedProps = [
  'header',
  'rawheader',
  'body',
]

const httpPlumbingProps = [
  'agent',
  'client',
  'connection',
  'event',
  'handle',
  'proxy',
  'req',
  'request',
  'res',
  'response',
  'server',
  'socket',
]

function dumpHttpReply(x) {
  if (!x) { return false }
  const { statusCode, statusMessage, body } = x
  if (body === undefined) { return false }
  if (statusCode === undefined) { return false }
  if (statusMessage === undefined) { return false }
  const meta = {}
  Object.keys(x).sort().forEach(function decideCopyProp(key) {
    if (key.startsWith('_')) { return }
    const lsk = lowerSingular(key)
    const val = x[key]
    if (httpExtractedProps.includes(lsk)) { return }
    if (httpPlumbingProps.includes(lsk)) {
      if (isObj(val)) {
        meta[key] = `… ${objToStr.call(val)} …`
        return
      }
    }
    meta[key] = val
  })
  const head = dumpHttpHeaders(x.rawHeaders || x.headers)
  return `${univeil.jsonify(deepSortObj(meta), null, 2)}\n\n${
    String(statusCode)} ${String(statusMessage)}\n${head}\n\n${body}`
}


function prettyPrint(x) {
  const httpDump = dumpHttpReply(x)
  if (httpDump) { return httpDump }
  if (Buffer.isBuffer(x)) {
    return `Buffer[${x.length}]: ${univeil.jsonify(x.toString('binary'))}`
  }
  const t = (x && typeof x)
  if (t === 'string') { return x }
  return util.format('%o', x)
}


export default prettyPrint
